echo "this will download all sensor data from new, local non-existent folders"
echo "please DONT enter archive.luftdaten.info folder when executing this"

wget -X `ls -dm  archive.luftdaten.info/*/  | sed 's./. .g' | sed  's/archive.luftdaten.info//g' | tr -d ' '  | tr -d '\n'` -r archive.luftdaten.info -N
