# Sensor data with some tools for luftdaten.info archive
extract the 7z archive so that the folder archive.luftdaten.info is in this (repository root) directory. If you manage it to add thousands of files via git, you are welcome to open a PR

## tools
The python script get_all_remote_file_list.py will follow all links in the directory listing and will output all links to all csv files in a file all_files.txt

The bash script get_new_sensor_data.sh will download all new sensor data from archive.luftdaten.info, from all locally non-existent folders. Please run this script from the repository root

