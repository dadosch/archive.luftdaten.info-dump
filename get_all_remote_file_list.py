#!/usr/bin/env python3
import requests
from itertools import chain
from bs4 import BeautifulSoup

# get url paths taken from https://stackoverflow.com/questions/11023530/python-to-list-http-files-and-directories

url = 'http://archive.luftdaten.info/'

def get_url_paths(url, ext='', params={}):
    response = requests.get(url, params=params)
    if response.ok:
        response_text = response.text
    else:
        return response.raise_for_status()
    soup = BeautifulSoup(response_text, 'html.parser')
    parent = [url + node.get('href') for node in soup.find_all('a') if node.get('href').endswith(ext)]
    return parent

subdirs = get_url_paths(url, "/")

allCSVFiles = []

# print out every csv file in all subdirectories.
# Use this file to get all files to download, then pipe them into a script, sort out the already existing ones
for subdir in subdirs:
    #print(subdir)
    allCSVFiles.append(get_url_paths(subdir, "csv"))
#    print(allCSVFiles)
    break
#    print(get_url_paths(subdir, "csv"))

#print(result)

outfile = open('all_files.txt','w')

# https://stackoverflow.com/questions/899103/writing-a-list-to-a-file-with-python
for file in chain.from_iterable(allCSVFiles):
    outfile.write("%s\n" % file)

outfile.close()



